const {
  sendMessageModel,
  getMessageModel,
  getMessageHistoryModel,
  checkingUserbyIdModel
} = require('../model/message')
const helper = require('../helper/response')

module.exports = {
  sendMessage: async (req, res) => {
    try {
      const { senderId, receiverId, message } = req.body
      const setData = {
        senderId,
        receiverId,
        message,
        createdAt: new Date()
      }
      const checkingUserbyId = await checkingUserbyIdModel(receiverId)
      if (checkingUserbyId.length < 1) {
        return helper.response(
          res,
          400,
          `User with id ${receiverId} not found!`
        )
      } else {
        const result = await sendMessageModel(setData)
        return helper.response(res, 200, 'Message sent!', result)
      }
    } catch (error) {
      console.log(error)
      return helper.response(res, 400, 'Bad Request', error)
    }
  },
  getMessage: async (req, res) => {
    try {
      const { id } = req.params
      const result = await getMessageModel(id)
      if (result.length > 0) {
        return helper.response(res, 200, 'Success get Message', result)
      } else {
        return helper.response(
          res,
          400,
          "Message not found! let's send a message to your friend"
        )
      }
    } catch (error) {
      console.log(error)
      return helper.response(res, 400, 'Bad Request', error)
    }
  },
  getMessageHistory: async (req, res) => {
    try {
      const { senderId, receiverId } = req.query
      const result = await getMessageHistoryModel(senderId, receiverId)
      if (result.length > 0) {
        return helper.response(res, 200, 'Success get Message History', result)
      } else {
        return helper.response(res, 400, 'Message history not found!')
      }
    } catch (error) {
      console.log(error)
      return helper.response(res, 400, 'Bad Request', error)
    }
  }
}
