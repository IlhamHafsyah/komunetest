const {
  cekFriendIdModel,
  tambahTemanModel,
  CheckContactModel
} = require('../model/friend')
const helper = require('../helper/response')

module.exports = {
  tambahTemanById: async (req, res) => {
    try {
      const { userId, friendId } = req.body
      const setData = {
        senderId: userId,
        receiverId: friendId,
        message: 'Hallo, saya baru saja menambahkan anda ke kontak saya'
      }
      const checkingId = await cekFriendIdModel(friendId)
      if (checkingId.length > 0) {
        const checkingContact = await CheckContactModel(friendId)
        if (checkingContact.length > 0) {
          return helper.response(
            res,
            400,
            `User with id ${friendId} already exist in your contact!`
          )
        } else {
          const result = await tambahTemanModel(setData)
          return helper.response(
            res,
            200,
            `Success add Friend by id ${friendId}`,
            result
          )
        }
      } else {
        return helper.response(res, 400, `User with id ${friendId} not found !`)
      }
    } catch (error) {
      console.log(error)
      return helper.response(res, 400, 'Bad Request', error)
    }
  }
}
