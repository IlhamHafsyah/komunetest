const router = require('express').Router()
const {
  sendMessage,
  getMessage,
  getMessageHistory
} = require('../controller/message')

router.post('/send', sendMessage)
router.get('/:id', getMessage)
router.get('/', getMessageHistory)

module.exports = router
