const router = require('express').Router()
const { tambahTemanById } = require('../controller/friend')

router.post('/add', tambahTemanById)

module.exports = router
