const connection = require('../config/mysql')

module.exports = {
  checkingUserbyIdModel: (receiverId) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'SELECT * FROM users WHERE id = ?',
        receiverId,
        (error, result) => {
          console.log(result)
          !error ? resolve(result) : reject(new Error(error))
        }
      )
    })
  },
  sendMessageModel: (setData) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'INSERT INTO chatinghistory SET ?',
        setData,
        (error, result) => {
          if (!error) {
            const newResult = {
              ...setData
            }
            resolve(newResult)
          } else {
            reject(new Error(error))
          }
        }
      )
    })
  },
  getMessageModel: (userId) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'SELECT senderId, message FROM users JOIN chatinghistory ON users.id = chatinghistory.receiverId WHERE id = ?',
        userId,
        (error, result) => {
          const message = [result]
          const newResult = [userId, ...message]
          !error ? resolve(newResult) : reject(new Error(error))
        }
      )
    })
  },
  getMessageHistoryModel: (senderId, receiverId) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM chatinghistory WHERE senderId = ${senderId} OR senderId = ${receiverId} AND receiverId = ${senderId} OR receiverId = ${receiverId}`,
        (error, result) => {
          !error ? resolve(result) : reject(new Error(error))
        }
      )
    })
  }
}
