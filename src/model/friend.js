const connection = require('../config/mysql')

module.exports = {
  cekFriendIdModel: (id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'SELECT * FROM users WHERE id = ?',
        id,
        (error, result) => {
          !error ? resolve(result) : reject(new Error(error))
        }
      )
    })
  },
  CheckContactModel: (id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'SELECT * FROM chatinghistory WHERE receiverId = ?',
        id,
        (error, result) => {
          !error ? resolve(result) : reject(new Error(error))
        }
      )
    })
  },
  tambahTemanModel: (setData) => {
    return new Promise((resolve, reject) => {
      connection.query(
        'INSERT INTO chatinghistory SET ?',
        setData,
        (error, result) => {
          if (!error) {
            const newResult = {
              ...setData
            }
            resolve(newResult)
          } else {
            reject(new Error(error))
          }
        }
      )
    })
  }
}
