const router = require('express').Router()
const addfriend = require('./routes/friend')
const message = require('./routes/message')

router.use('/friend', addfriend)
router.use('/message', message)

module.exports = router
